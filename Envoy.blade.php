@servers(['web' => 'deployer@193.124.206.231'])

@setup
    $repository = 'git@gitlab.com:VyalovAlexander/idriver.git';
    $releases_dir = '/var/www/app/releases';
    $app_dir = '/var/www/app';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
    @endsetup

    @story('deploy')
    clone_repository
    run_composer
    build_dependencies
    linking_storage_and_env
    migrate_db
    link_release_dir
    @endstory

    @task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
    @endtask

    @task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
    @endtask

    @task('build_dependencies')
    echo "Building dependencies"
    cd {{ $new_release_dir }}
    npm cache clean --force
    npm install --production | true ; echo $?
    npm audit fix | true ; echo $?
    npm run prod | true ; echo $?
    @endtask

    @task('linking_storage_and_env')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
    @endtask

    @task('migrate_db')
    echo "Migrating database"
    cd {{ $new_release_dir }}
    php artisan config:cache
    php artisan migrate --force
    @endtask

    @task('link_release_dir')
    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
    @endtask

    @task('restart_php_fpm')
    echo 'Restart php fpm'
    service php7.1-fpm restart
    @endtask