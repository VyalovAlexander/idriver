#!/usr/bin/env bash

composer install
chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache
rm -rv public/storage
php artisan storage:link
php artisan cache:clear
php artisan migrate
php-fpm