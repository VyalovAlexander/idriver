<?php

namespace App\Providers;

use App\Components\Telegram\Services\Telegram;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

class TelegramServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Telegram::class, function ($app) {
            return new Telegram(
                $app->make(ClientInterface::class),
                config('telegram.bot_token'),
                config('telegram.bot_url'),
                config('telegram.bot_name'),
                config('telegram.url')
            );
        });
    }
}
