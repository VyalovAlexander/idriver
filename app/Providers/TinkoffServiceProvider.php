<?php
/**
 * Created by PhpStorm.
 * User: vyalov
 * Date: 02.06.19
 * Time: 14:59
 */

namespace App\Providers;

use App\Components\Aggregator\Repositories\BusinessGettRepository;
use App\Components\Aggregator\Repositories\CacheGettRepositoryDecorator;
use App\Components\Aggregator\Repositories\ComfortGettRepository;
use App\Components\Aggregator\Repositories\HttpGettRepository;
use App\Components\Bank\Repositories\TinkoffBankRepository;
use App\Components\Bank\Repositories\TinkoffBankRepositoryInterface;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

class TinkoffServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TinkoffBankRepositoryInterface::class, function ($app) {
            $rep = new TinkoffBankRepository(
                $app->make(ClientInterface::class)

            );
            return $rep;
        });

    }
}