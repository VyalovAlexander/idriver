<?php

namespace App\Providers;

use App\Components\Aggregator\Repositories\HttpYandextaxiRepository;
use App\Components\Aggregator\Repositories\HttpYandexV7Repository;
use App\Components\Aggregator\Repositories\YandextaxiRepository;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

class YandexServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(YandextaxiRepository::class, function ($app) {
            return new HttpYandexV7Repository(
                $app->make(ClientInterface::class),
                config('yandextaxi.api_url_v1'),
                config('yandextaxi.api_url_v2'),
                config('yandextaxi.client_id'),
                config('yandextaxi.park_id'),
                config('yandextaxi.api_key')
            );
        });
    }
}
