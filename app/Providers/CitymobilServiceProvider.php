<?php

namespace App\Providers;

use App\Components\Aggregator\Repositories\CitymobilRepository;
use App\Components\Aggregator\Repositories\HttpCitymobilRepository;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

class CitymobilServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CitymobilRepository::class, function ($app) {
            return new HttpCitymobilRepository(
                $app->make(ClientInterface::class),
                config('citymobil.api_url'),
                config('citymobil.token')
            );
        });
    }
}
