<?php

namespace App\Providers;

use App\Components\Aggregator\Repositories\BusinessGettRepository;
use App\Components\Aggregator\Repositories\CacheGettRepositoryDecorator;
use App\Components\Aggregator\Repositories\ComfortGettRepository;
use App\Components\Aggregator\Repositories\HttpGettRepository;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

class GettServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ComfortGettRepository::class, function ($app) {
            $comfortRep = new HttpGettRepository(
                $app->make(ClientInterface::class),
                config('gett.api_url'),
                config('gett.comfort_login'),
                config('gett.comfort_password')
            );
            return $comfortRep;
        });

        $this->app->bind(BusinessGettRepository::class, function ($app) {
            $businessRep = new HttpGettRepository(
                    $app->make(ClientInterface::class),
                    config('gett.api_url'),
                    config('gett.business_login'),
                    config('gett.business_password')
            );
            return $businessRep;
        });
    }
}
