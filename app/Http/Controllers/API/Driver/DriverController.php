<?php

namespace App\Http\Controllers\API\Driver;

use App\Components\Aggregator\Repositories\CitymobilRepository;
use App\Components\Aggregator\Repositories\YandextaxiRepository;
use App\Components\Aggregator\Resources\DriverBalanceResource;
use App\Components\Driver\Repositories\DriverRepository;
use App\Components\Transaction\Resources\TransactionResource;
use App\Components\Transaction\Repositories\TransactionRepository;
use App\Http\Controllers\Controller;



class DriverController extends Controller
{


    public function transactions(TransactionRepository $transactionRepository, DriverRepository $driverRepository)
    {
        $transactionRepository->filter();
        $transactions = $transactionRepository->currentDriverTransactions($driverRepository);
        return TransactionResource::collection($transactions);
    }

    public function balance(YandextaxiRepository $yandextaxiRepository, CitymobilRepository $citymobilRepository, DriverRepository $driverRepository)
    {
        $driver = $driverRepository->getDriver();

        $balance_yandex = $yandextaxiRepository->balance($driver);
        $balance_citymobil = $citymobilRepository->balance($driver);
        $balance_gett = $driver->gett_balance;

        return DriverBalanceResource::make([
            'yandex' => $balance_yandex,
            'citymobil' => $balance_citymobil,
            'gett' => $balance_gett
        ]);
    }

}
