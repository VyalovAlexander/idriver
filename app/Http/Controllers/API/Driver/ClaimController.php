<?php

namespace App\Http\Controllers\API\Driver;

use App\Components\Claims\Requests\CreateClaimRequest;
use App\Components\Claims\Resources\ClaimsResource;
use App\Components\Driver\Repositories\DriverRepository;
use App\Components\Claims\Repositories\ClaimsRepository;
use App\Http\Controllers\Controller;
use App\Mail\NewClaimMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class ClaimController extends Controller
{
    private $claimRepository;

    private $driverRepository;

    public function __construct(ClaimsRepository $claimsRepository, DriverRepository $driverRepository)
    {
        parent::__construct();
        $this->claimRepository = $claimsRepository;
        $this->driverRepository = $driverRepository;
    }

    public function index()
    {
        $this->claimRepository->filter();
        $claims = $this->claimRepository->currentDriverClaims($this->driverRepository);
        return ClaimsResource::collection($claims);
    }

    public function create(CreateClaimRequest $request)
    {
        $attr = $request->all();
        $attr['driver_id'] = $this->driverRepository->getDriverId();
        $claim = $this->claimRepository->create($attr);
        $user = Auth::user();

        Mail::to('idrivermessages@yandex.ru')->send(new NewClaimMail($claim, $user));

        return response()->json();
    }
}