<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Components\User\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request){
        $field = filter_var(request('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        if(Auth::attempt([$field => request('login'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('API')-> accessToken;
            $success['profile'] = [
                'name' => $user->name,
                'roles' => $user->rolesNames()
            ];

            return response()->json($success, 200);
        }

        return response()->json(['error'=>'Unauthorised'], 401);

    }

    public function logout(Request $request){
        $request->user()->token()->revoke();
        return response()->json(null, 204);
    }
}