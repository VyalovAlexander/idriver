<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Components\User\Requests\RegisterRequest;
use App\Components\Station\Repositories\StationRepository;
use App\Components\User\Entities\User;

class RegisterController extends Controller
{

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterRequest $request, User $user)
    {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = $user->fill($input);
        $user->save();
        $success['token'] = $user->createToken('API')->accessToken;
        return response()->json($success, 200);
    }


}