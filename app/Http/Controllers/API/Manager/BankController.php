<?php

namespace App\Http\Controllers\API\Manager;

use App\Components\Bank\Entities\Bank;
use App\Components\Bank\Repositories\TinkoffBankRepository;
use App\Components\Bank\Requests\SendToBankRequest;
use App\Components\Claims\Repositories\ClaimsRepository;
use App\Components\Driver\Repositories\DriverRepository;
use App\Http\Controllers\Controller;

class BankController extends Controller
{
    private $claimRepository;

    private $bankRepository;

    public function __construct(TinkoffBankRepository $tinkoffBankRepository, ClaimsRepository $claimsRepository, DriverRepository $repository)
    {
        $this->claimRepository = $claimsRepository;
        $this->bankRepository = $tinkoffBankRepository;
    }

    public function sendToBank(SendToBankRequest $request) {
        $claim = $this->claimRepository->with(['driver', 'driver.user'])->find($request->id);
        $banks = Bank::all();
        $bank = $banks->first();
        if ($bank) {
            $token = $this->bankRepository->login($bank);
            $id = $this->bankRepository->save($bank, $token, $claim->driver, $claim, (float)$request->amount);
            return response()->json([
                'id' => $id
            ]);
        }

        return response()->json();


    }


}