<?php

namespace App\Http\Controllers\API\Manager;

use App\Components\Driver\Criteria\DriverFilterCriteria;
use App\Components\Transaction\Criteria\DriversCriteria;
use App\Components\Transaction\Criteria\PaymentCriteria;
use App\Components\Transaction\Repositories\TransactionRepository;
use App\Components\Transaction\Requests\PaymentReportRequest;
use App\Components\Transaction\Resources\TransactionResource;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function payments(PaymentReportRequest $request, TransactionRepository $transactionRepository)
    {
        $transactionRepository->pushCriteria(new PaymentCriteria((bool)$request->only_payments));
        $transactionRepository->pushCriteria(new DriverFilterCriteria($request));
        $transactionRepository->filter();
        $transactions = $transactionRepository->with(['driver', 'driver.user'])->paymentsReport();
        return TransactionResource::collection($transactions);
    }
}