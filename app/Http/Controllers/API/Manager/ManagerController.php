<?php

namespace App\Http\Controllers\API\Driver;

use App\Components\Station\Repositories\StationRepository;
use App\Components\Transaction\Resources\TransactionResource;
use App\Components\Transaction\Repositories\TransactionRepository;
use App\Components\User\Criteria\CurrentManagerCriteria;
use App\Components\User\Criteria\CurrentUserCriteria;
use App\Http\Controllers\Controller;

class ManagerController extends Controller
{


    public function drivers() {

    }

    public function transactions(TransactionRepository $transactionRepository) {
        $transactionRepository->filter();
        $transactionRepository->pushCriteria(CurrentUserCriteria::class);
        $transactions = $transactionRepository->with(['station', 'ride'])->orderBy('updated_at', 'DESC')->paginate();
        return TransactionResource::collection($transactions);
    }


}