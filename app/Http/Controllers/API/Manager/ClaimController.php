<?php

namespace App\Http\Controllers\API\Manager;


use App\Components\Claims\Requests\ClaimRequest;
use App\Components\Claims\Requests\UpdateClaimRequest;
use App\Components\Claims\Resources\ClaimsResource;
use App\Components\Claims\Repositories\ClaimsRepository;
use App\Components\Driver\Repositories\DriverRepository;
use App\Http\Controllers\Controller;

class ClaimController extends Controller
{
    private $claimRepository;

    public function __construct(ClaimsRepository $claimsRepository, DriverRepository $repository)
    {
        $this->claimRepository = $claimsRepository;
    }

    public function index()
    {
        $this->claimRepository->filter();
        $claims = $this->claimRepository->with(['driver', 'driver.user'])->orderBy('created_at', 'DESC')->paginate();

        return ClaimsResource::collection($claims);
    }

    public function update(UpdateClaimRequest $request)
    {
        $claim = $this->claimRepository->update(array_merge($request->all(), ['closed' => true]), $request->get('id'));

        return ClaimsResource::make($claim);
    }

    public function claim(ClaimRequest $request)
    {
        $claim = $this->claimRepository->find($request->id);

        return ClaimsResource::make($claim);
    }

}