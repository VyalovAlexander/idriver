<?php

namespace App\Http\Controllers\API\Manager;


use App\Components\Aggregator\Repositories\BusinessGettRepository;
use App\Components\Aggregator\Repositories\ComfortGettRepository;
use App\Components\Aggregator\Repositories\GettRepository;
use App\Components\Aggregator\Repositories\YandextaxiRepository;
use App\Components\Aggregator\Resources\CitymobilDriverResource;
use App\Components\Aggregator\Resources\DriverBalanceResource;
use App\Components\Aggregator\Resources\GettDriverResource;
use App\Components\Aggregator\Resources\YandextaxiDriverResource;
use App\Components\Driver\Criteria\DriverUserCriteria;
use App\Components\Driver\Entities\Driver;
use App\Components\Driver\Resources\DriverResource;
use App\Components\Aggregator\Repositories\CitymobilRepository;
use App\Components\Driver\Repositories\DriverRepository;
use App\Components\Transaction\Entities\Transaction;
use App\Components\Transaction\Repositories\TransactionRepository;
use App\Components\Transaction\Requests\ChangeBalanceRequest;
use App\Components\User\Criteria\UserNameCriteria;
use App\Components\User\Repositories\UserRepository;
use App\Components\User\Requests\CreateDriverRequest;
use App\Components\User\Requests\GetDriverRequest;
use App\Components\User\Requests\UpdateDriverRequest;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DriverController extends Controller
{

    private $driverRepository;

    private $userRepository;

    private $citymobilRepository;

    private $yandextaxiRepository;

    private $comfortGettRepository;

    private $businessGettRepository;

    private $transactionRepository;


    public function __construct(DriverRepository $driverRepository, UserRepository $userRepository, CitymobilRepository $citymobilRepository, YandextaxiRepository $yandextaxiRepository, ComfortGettRepository $comfortGettRepository, BusinessGettRepository $businessGettrepository, TransactionRepository $transactionRepository)
    {
        parent::__construct();
        $this->driverRepository = $driverRepository;
        $this->userRepository = $userRepository;
        $this->citymobilRepository = $citymobilRepository;
        $this->yandextaxiRepository = $yandextaxiRepository;
       $this->comfortGettRepository = $comfortGettRepository;
       $this->businessGettRepository = $businessGettrepository;
       $this->transactionRepository = $transactionRepository;
    }

    public function create(CreateDriverRequest $createDriverRequest) {
        $userAttributes = $createDriverRequest->all(['name', 'password', 'email', 'phone']);
        $userAttributes['password'] = bcrypt($userAttributes['password']);
        $user = $this->userRepository->create($userAttributes);
        $user->attachRole(3);
        $driverAttributes = $createDriverRequest->all(['citymobil_id', 'yandex_id', 'gett_id', 'inn',
            'kpp',
            'bank_acnt',
            'bank_bik',
            'account_number',
            'payment_purpose']);
        $driverAttributes['deposit'] = 0;
        $driverAttributes['user_id'] = $user->id;
        $driverAttributes['citymobil_id'] = $driverAttributes['citymobil_id'] ?: '';
        $driverAttributes['yandex_id'] = $driverAttributes['yandex_id'] ?: '';
        $driverAttributes['gett_id'] = $driverAttributes['gett_id'] ?: '';
        $driverAttributes['inn'] = $driverAttributes['inn'] ?: '';
        $driverAttributes['kpp'] = $driverAttributes['kpp'] ?: '';
        $driverAttributes['account_number'] = $driverAttributes['bank_acnt'] ?: '';
        $driverAttributes['bank_bik'] = $driverAttributes['bank_bik'] ?: '';
        $driverAttributes['payment_purpose'] = $driverAttributes['payment_purpose'] ?: '';

        $this->driverRepository->create($driverAttributes);

        return response()->json();
    }


    public function update(UpdateDriverRequest $updateDriverRequest) {
        $driver = $this->driverRepository->with(['user'])->find($updateDriverRequest->get('id'));
        $user = $driver->user;
        $userAttributes = $updateDriverRequest->all(['name', 'email', 'phone']);
        if (!empty($updateDriverRequest->get('password'))) {
            $userAttributes['password'] = bcrypt($updateDriverRequest->get('password'));
        }
        $user = $this->userRepository->update($userAttributes, $user->id);
        $driverAttributes = $updateDriverRequest->all(['citymobil_id', 'yandex_id', 'gett_id', 'inn',
            'kpp',
            'bank_acnt',
            'bank_bik',
            'account_number',
            'payment_purpose']);
        $driverAttributes['citymobil_id'] = $driverAttributes['citymobil_id'] ?: '';
        $driverAttributes['yandex_id'] = $driverAttributes['yandex_id'] ?: '';
        $driverAttributes['gett_id'] = $driverAttributes['gett_id'] ?: '';
        $driverAttributes['inn'] = $driverAttributes['inn'] ?: '';
        $driverAttributes['kpp'] = $driverAttributes['kpp'] ?: '';
        $driverAttributes['account_number'] = $driverAttributes['bank_acnt'] ?: '';
        $driverAttributes['bank_bik'] = $driverAttributes['bank_bik'] ?: '';
        $driverAttributes['payment_purpose'] = $driverAttributes['payment_purpose'] ?: '';
        $this->driverRepository->update($driverAttributes, $driver->id);

        return response()->json();
    }


    public function index(Request $request, UserRepository $userRepository) {
        if ($request->has('name') && (string)$request->get('name') !== '') {
            $userRepository->pushCriteria(new UserNameCriteria((string)$request->get('name')));
            $users_ids = $userRepository->all(['id'])->transform(function ($item, $key) {
                return $item->id;
            })->toArray();
            $this->driverRepository->pushCriteria(new DriverUserCriteria($users_ids));
            $drivers = $this->driverRepository->with(['user'])->paginate();
        } else {
            $drivers = $this->driverRepository->with(['user'])->paginate();
        }

        return DriverResource::collection($drivers);
    }

    public function all() {
        $drivers = $this->driverRepository->with(['user'])->all();
        return DriverResource::collection($drivers);
    }

    public function citymobilDrivers() {
        $drivers = $this->citymobilRepository->drivers();
        return CitymobilDriverResource::collection(collect($drivers));
    }

    public function yandextaxiDrivers() {
        $drivers = $this->yandextaxiRepository->drivers();
        return YandextaxiDriverResource::collection(collect($drivers));
    }

    public function gettDrivers() {
        $driversComfort = $this->comfortGettRepository->getDrivers();
        $driversBusiness = $this->businessGettRepository->getDrivers();

        return GettDriverResource::collection(collect(array_merge($driversComfort, $driversBusiness)));
    }

    public function driver(GetDriverRequest $request) {
        $driver = $this->driverRepository->with(['user'])->find($request->get('id'));
        return DriverResource::make($driver);
    }

    public function changeCityMobilBalance(ChangeBalanceRequest $request) {
        $driver = $this->driverRepository->find($request->driver_id);
        $result = $this->citymobilRepository->changeBalance($driver, -(float)$request->transfer);

        if ($result) {
            DB::beginTransaction();

            try {
                $driver = $this->driverRepository->find($request->driver_id);
                $transaction = new Transaction([
                    'amount' => -(float)$request->transfer,
                    'driver_id' => $driver->id,
                    'aggregator_id' => 2,
                    'comment' => 'Изменение баланса',
                    'date' => Carbon::now()->addHour(3),
                    'station_transaction' => true

                ]);
                $transaction->save();
                DB::commit();
                $result = true;
                // all good
            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
            }
        }

        return response()->json(['success' => $result]);
    }

    public function changeYandexBalance(ChangeBalanceRequest $request) {
        $driver = $this->driverRepository->find($request->driver_id);
        $result = $this->yandextaxiRepository->changeBalance($driver, -(float)$request->transfer);

        if ($result) {
            DB::beginTransaction();

            try {
                $driver = $this->driverRepository->find($request->driver_id);
                $transaction = new Transaction([
                    'amount' => -(float)$request->transfer,
                    'driver_id' => $driver->id,
                    'aggregator_id' => 1,
                    'comment' => 'Изменение баланса',
                    'date' => Carbon::now()->addHour(3),
                    'station_transaction' => true

                ]);
                $transaction->save();
                DB::commit();
                $result = true;
                // all good
            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
            }
        }

        return response()->json(['success' => $result]);
    }

    public function changeGettBalance(ChangeBalanceRequest $request) {

        $result = false;
//
        DB::beginTransaction();

        try {
            $driver = $this->driverRepository->find($request->driver_id);
            $driver->gett_balance = $driver->gett_balance - $request->transfer;
            $driver->save();
            $transaction = new Transaction([
                'amount' => -(float)$request->transfer,
                'driver_id' => $driver->id,
                'aggregator_id' => 3,
                'comment' => 'Изменение баланса',
                'date'=> Carbon::now()->addHour(3),
                'station_transaction' => true

            ]);
            $transaction->save();
            DB::commit();
            $result = true;
            // all good
       } catch (\Exception $e) {
         DB::rollback();
            // something went wrong
        }

        return response()->json(['success' => $result]);
    }

    public function balance(GetDriverRequest $request)
    {
        $driver = $this->driverRepository->find($request->id);

        $balance_yandex = $this->yandextaxiRepository->balance($driver);
        $balance_citymobil = $this->citymobilRepository->balance($driver);
        $balance_gett = $driver->gett_balance;

        return DriverBalanceResource::make([
            'yandex' => $balance_yandex,
            'citymobil' => $balance_citymobil,
            'gett' => $balance_gett
        ]);

    }

}