<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function profile(){
        return response()->json([
            'name' => $this->user->name,
            'balance' => 0,
            'roles' => $this->user->rolesNames
        ]);
    }


}