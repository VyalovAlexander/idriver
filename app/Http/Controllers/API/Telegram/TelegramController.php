<?php

namespace App\Http\Controllers\API\Telegram;

use App\Components\Telegram\Services\Telegram;
use App\Http\Controllers\Controller;
use App\Notifications\TextTelegramNotification;
use Illuminate\Contracts\Hashing\Hasher;

class TelegramController extends Controller
{

   private $telegram;

   public function __construct(Telegram $telegram)
   {
       parent::__construct();
       $this->telegram = $telegram;

   }

    public function start() {
       $token = md5($this->user->getAuthPassword());
       $this->user->startTelegram($token);
       $this->user->save();

       return response()->json([
           'link' => $this->telegram->link($this->user->telegram_token)
       ]);

   }

   public function verify() {
       $chaId = $this->telegram->getChatIdByToken($this->user->telegram_token);
       if($chaId !== '') {
           $this->user->chat_id = $chaId;
       }
       $this->user->save();
       $this->user->notify(new TextTelegramNotification('Вы подключились!'));

       return response()->json();
   }

   public function connected() {
       return response()->json([
           'connected' => $this->user->telegramConnected(),
       ]);
   }
}