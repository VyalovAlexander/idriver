<?php

namespace App\Console\Commands;

use App\Components\Aggregator\Repositories\CitymobilRepository;
use App\Components\Driver\Repositories\DriverRepository;
use App\Components\Transaction\Entities\Transaction;
use App\Components\Transaction\Repositories\TransactionRepository;
use App\Notifications\TextTelegramNotification;
use Illuminate\Console\Command;

class CitymobilPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'citymobil:payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $driverRepository;

    private $citymobilRepository;

    private $transactionRepository;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DriverRepository $driverRepository, CitymobilRepository $citymobilRepository, TransactionRepository $transactionRepository)
    {
        $this->driverRepository = $driverRepository;
        $this->citymobilRepository = $citymobilRepository;
        $this->transactionRepository = $transactionRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $drivers = $this->driverRepository->with(['user'])->all();
        foreach ($drivers as $driver) {
            if (!empty($driver->citymobil_id)) {
                $transactions = $this->citymobilRepository->transactions($driver);
                $citymobil_payment_ids = array_keys($transactions);
                $citymobil_payment_ids_already_saved = $this->transactionRepository->findWhereIn('citymobil_payment_id', $citymobil_payment_ids, ['citymobil_payment_id'])->transform(function ($item, $key) {
                    return $item->citymobil_payment_id;
                })->toArray();
                foreach ($transactions as $transaction) {
                    if (!in_array($transaction->citymobil_payment_id, $citymobil_payment_ids_already_saved)) {
                        $this->transactionRepository->updateOrCreate(
                            [
                                'citymobil_payment_id' => $transaction->citymobil_payment_id
                            ],
                            [
                                'driver_id' => $transaction->driver_id,
                                'amount' => $transaction->amount,
                                'aggregator_id' => $transaction->aggregator_id,
                                'comment' => $transaction->comment,
                                'citymobil_payment_id' => $transaction->citymobil_payment_id,
                                'citymobil_payment_id_hash' => $transaction->citymobil_payment_id_hash,
                                'citymobil_payment_id_type' => $transaction->citymobil_payment_id_type,
                                'aggregator_commission' => $transaction->aggregator_commission,
                                'station_commission' => $transaction->station_commission,
                                'date' => $transaction->date,
                            ]
                        );
                        if ($driver->user->telegramConnected()) {
                            $driver->user->notify(new TextTelegramNotification("Новая транзакция Ситимобил, сумма {$transaction->amount}, дата {$transaction->date}, $transaction->comment"));
                        }

                    }
                }
            }
        }
    }
}
