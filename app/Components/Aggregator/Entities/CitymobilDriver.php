<?php
namespace App\Components\Aggregator\Entities;

use Illuminate\Database\Eloquent\Model;

class CitymobilDriver extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'citymobil_id'
    ];
}