<?php
namespace App\Components\Aggregator\Entities;

use Illuminate\Database\Eloquent\Model;

class YandexDriver extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'yandex_id'
    ];
}