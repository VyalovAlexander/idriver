<?php
namespace App\Components\Aggregator\Criteria;

use App\Support\FilterCriteria;
use Prettus\Repository\Contracts\RepositoryInterface;

class AggreagtorFilterCriteria extends FilterCriteria
{
    protected function filterName(): string
    {
        return 'aggregators';
    }

    public function applyValue($model, RepositoryInterface $repository)
    {
        $model = $model->whereIn('aggregator_id',  explode(",", $this->getValue()));

        return $model;
    }


}
