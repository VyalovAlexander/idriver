<?php

namespace App\Components\Aggregator\Utils;

trait AggregatorTrait
{
    public $aggregatorName = '';

    public $aggregatorImage = '';

    public function fillAggregator()
    {
        if (filled($this->agggregator_id)) {
            $aggregator = config('aggregator')[$this->agggregator_id];
            $this->attributes['aggregator_image'] = $aggregator['image'];
            $this->attributes['aggregator_name'] = $aggregator['name'];
        }
    }
}