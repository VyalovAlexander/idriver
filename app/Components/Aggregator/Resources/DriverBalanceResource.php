<?php

namespace App\Components\Aggregator\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverBalanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'common' => round(round($this['citymobil'], 2) + round($this['yandex'], 2) + round($this['gett'], 2),   2),
            'yandex' => round($this['yandex'], 2),
            'citymobil' => round($this['citymobil'], 2),
            'gett' => round($this['gett'], 2)
        ];
    }
}