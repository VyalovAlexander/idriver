<?php

namespace App\Components\Aggregator\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CitymobilDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->citymobil_id,
            'name' => $this->name
        ];
    }
}