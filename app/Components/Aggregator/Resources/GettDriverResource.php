<?php
/**
 * Created by PhpStorm.
 * User: vyalov
 * Date: 30.03.19
 * Time: 10:37
 */

namespace App\Components\Aggregator\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GettDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->gett_id,
            'name' => $this->name
        ];
    }
}