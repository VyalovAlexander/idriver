<?php

namespace App\Components\Aggregator\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class YandextaxiDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->yandex_id,
            'name' => $this->name
        ];
    }
}