<?php

namespace App\Components\Aggregator\Repositories;


use App\Components\Aggregator\Entities\CitymobilDriver;
use App\Components\Aggregator\Entities\YandexDriver;
use App\Components\Driver\Entities\Driver;
use App\Components\Transaction\Entities\Transaction;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Carbon;

class HttpYandextaxiRepository implements YandextaxiRepository
{
    private $http;
    private $url;
    private $token;

    public function __construct(ClientInterface $http, string $url, string $token)
    {
        $this->http = $http;
        $this->url = $url;
        $this->token = $token;

    }

    public function drivers(): array
    {
        $drivers = [];
        $response = $this->http->request('GET', "{$this->url}/driver/list?apikey={$this->token}");
        $message = json_decode($response->getBody()->getContents());
        if (isset($message->drivers) && is_object($message->drivers)) {
            foreach ($message->drivers as $key => $driver) {
                $surname = isset($driver->Surname) && !empty($driver->Surname) ? $driver->Surname : '';
                $attributes = [];
                $attributes['name'] = "{$driver->LastName} {$driver->FirstName}  {$surname}";
                $attributes['yandex_id'] = "{$key}";
                $drivers[] = new YandexDriver($attributes);
            }
        }

        return $drivers;
    }

    public function balance(Driver $driver): float
    {
        $balance = 0.0;
        if (!empty($driver->yandex_id)) {
            $response = $this->http->request('GET', "{$this->url}/driver/get?apikey={$this->token}&id={$driver->yandex_id}");
            $message = json_decode($response->getBody()->getContents());
            if (isset($message->balance)) {
                $balance = (float)$message->balance;
            }
        }
        return $balance;
    }

    public function changeBalance(Driver $driver, float $transfer): bool
    {
        $balance = 0.0;
        if (!empty($driver->yandex_id)) {
            $type = $transfer >= 0 ? 'plus' : 'minus';
            $response = $this->http->request('GET', "{$this->url}/driver/balance/{$type}?apikey={$this->token}&driver={$driver->yandex_id}&sum={$transfer}&group=1&description=через_приложение");
            if ($response->getStatusCode() == 200) {
                return true;
            }
        }
        return false;
    }


}