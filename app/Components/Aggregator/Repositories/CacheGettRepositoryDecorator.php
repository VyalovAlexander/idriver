<?php

namespace App\Components\Aggregator\Repositories;

use App\Components\Aggregator\Entities\GettDriver;
use App\Components\Aggregator\Resources\GettDriverResource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class CacheGettRepositoryDecorator implements BusinessGettRepository, ComfortGettRepository
{
    private $repository;

    private $key = '';

    private $token;

    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    public function setToken(string $token): void
    {
        $this->repository->setToken($token);
        $this->token = $token;
    }


    public function __construct(GettRepository $repository)
    {
        $this->repository = $repository;
    }


    public function getToken()
    {
        $token = Cache::get($this->key);
        if ($token) {
            $this->setToken($token);
            return $token;
        }
        $token = $this->repository->getToken();

        if ($token) {
            Cache::add($this->key, $token, 5);
            $this->setToken($token);
            return $token;
        }

        return null;
    }

    public function getDrivers(bool $force = false): array
    {

        if ($force) {
            $drivers = $this->repository->getDrivers(true);

            Cache::add($this->key . "_drivers", collect($drivers)->toJson(), 5);
            return $drivers;
        }

        $json_drivers = Cache::get($this->key . "_drivers");
        if ($json_drivers) {
            $array_drivers = json_decode($json_drivers);
            $drivers = [];
            foreach ($array_drivers as $driver) {
                $drivers[] = new GettDriver([
                    'name' => $driver->name,
                    'gett_id' => $driver->gett_id
                ]);
            }

            return $drivers;
        }

        $this->getToken();
        $drivers = $this->repository->getDrivers($force);
        Cache::add($this->key . "_drivers", collect($drivers)->toJson(), 5);
        return $drivers;
    }

    public function askForReport(Carbon $from, Carbon $to)
    {
        $uid = Cache::get($this->key . "_report");
        if ($uid) {
            return $uid;
        }

        $this->getToken();
        $uid = $this->repository->askForReport($from, $to);
        if ($uid) {
            Cache::add($this->key. "_report", $uid, 5);
            return $uid;
        }

        return null;

    }

    public function getReport(string $uid)
    {
        $this->getToken();
        return $this->repository->getReport($uid);
    }




}