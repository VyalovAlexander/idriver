<?php

namespace App\Components\Aggregator\Repositories;

use App\Components\Aggregator\Entities\GettDriver;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class HttpGettRepository implements ComfortGettRepository, BusinessGettRepository
{
    private $http;
    private $login;
    private $password;
    private $token = '';
    private $url;

    public function __construct(ClientInterface $http, string $url, string $login, string $password)
    {
        $this->http = $http;
        $this->url = $url;
        $this->login = $login;
        $this->password = $password;

    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }


    public function getToken()
    {
        if ($this->token) {
            return $this->token;
        }

        $response = $this->http->request('GET', "{$this->url}/auth?login={$this->login}&password={$this->password}");
        $message = json_decode($response->getBody()->getContents());
        if (isset($message->result) && $message->result && isset($message->access_token)) {
            $this->token = $message->access_token;
            return $message->access_token;
        }

        return null;
    }

    public function getDrivers(bool $force = false): array
    {
        $drivers = [];

        try {
            $token = $this->getToken();
            if ($token) {
                $response = $this->http->request('POST', "{$this->url}/drivers/get",
                    [
                        'headers' => [
                            "Authorization" => "Bearer {$token}",
                        ]
                    ]);
                $message = json_decode($response->getBody()->getContents());
                if (isset($message->result) && $message->result && isset($message->data) && is_array($message->data)) {
                    foreach ($message->data as $driver) {
                        $drivers[] = new GettDriver([
                            'name' => $driver->name,
                            'gett_id' => $driver->driver_id
                        ]);
                    }
                }


            }
        } catch (GuzzleException $exception) {
            //echo $exception->getMessage();
            $a = 1;
        }

        return $drivers;

    }

    public function askForReport(\Carbon\Carbon $from, \Carbon\Carbon $to)
    {


            $token = $this->getToken();


            if ($token) {
                $response = $this->http->request('POST', "{$this->url}/dbr/create",
                    [
                        'headers' => [
                            "Authorization" => "Bearer {$token}",
                        ],
                        'json' => [
                            'from' => $from->format('Y-m-d H:i:s'),
                            'to' => $to->format('Y-m-d H:i:s')
                        ],
                        'http_errors' => false
                    ]);
                $message = json_decode($response->getBody()->getContents());
                //Log::debug(json_encode($message));
                if (isset($message->uid)) {
                    return $message->uid;
                } elseif (isset($message->result) && !$message->result && isset($message->message)) {
                    $explode_result = explode('uid', $message->message);
                    if (count($explode_result) > 1) {
                        $explode_result_after = explode(':', $explode_result[1]);
                        if (count($explode_result_after) > 1) {
                            return trim($explode_result_after[1]);
                        } else {
                            return trim($explode_result[1]);
                        }

                    }
                }
            }


        return null;
    }

    public function getReport(string $uid)
    {
        $token = $this->getToken();


        if ($token) {
            $response = $this->http->request('POST', "{$this->url}/dbr/get",
                [
                    'headers' => [
                        "Authorization" => "Bearer {$token}",
                    ],
                    'json' => [
                        'uid' => $uid
                    ],
                    'http_errors' => false
                ]);
            $message = json_decode($response->getBody()->getContents());
           // Log::debug(json_encode($message));
            if (isset($message->result) && $message->result && isset($message->data) && isset($message->data->rides) && is_array($message->data->rides)) {
                $rides = [];
                foreach ($message->data->rides as $ride) {
                    $rides[$ride->order_id] = $ride;
                }

                return $rides;
            }


        }


        return null;
    }


}