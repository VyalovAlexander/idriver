<?php

namespace App\Components\Aggregator\Repositories;

use App\Components\Aggregator\Entities\CitymobilDriver;
use App\Components\Driver\Entities\Driver;

interface YandextaxiRepository
{
    /**
     * @return CitymobilDriver[]
     */
    public function drivers(): array;

    public function balance(Driver $driver): float;

    public function changeBalance(Driver $driver, float $transfer): bool;
}