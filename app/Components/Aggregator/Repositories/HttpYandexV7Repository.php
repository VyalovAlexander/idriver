<?php
/**
 * Created by PhpStorm.
 * User: vyalov
 * Date: 08.03.20
 * Time: 12:40
 */

namespace App\Components\Aggregator\Repositories;

use App\Components\Aggregator\Entities\CitymobilDriver;
use App\Components\Aggregator\Entities\YandexDriver;
use App\Components\Driver\Entities\Driver;
use App\Components\Transaction\Entities\Transaction;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Carbon;
use Ramsey\Uuid\Uuid;

/**
 * Class HttpYandexV7Repository
 * @package App\Components\Aggregator\Repositories
 */
class HttpYandexV7Repository implements YandextaxiRepository
{

    /**
     * @var ClientInterface
     */
    private $http;
    /**
     * @var string
     */
    private $urlV1;
    /**
     * @var string
     */
    private $urlV2;
    /**
     * @var string
     */
    private $clientId;
    /**
     * @var string
     */
    private $parkId;
    /**
     * @var string
     */
    private $authKey;

    /**
     * HttpYandexV7Repository constructor.
     * @param ClientInterface $http
     * @param string $urlV1
     * @param string $urlV2
     * @param string $clientId
     * @param string $parkId
     * @param string $authKey
     */
    public function __construct(
        ClientInterface $http,
        string $urlV1,
        string $urlV2,
        string $clientId,
        string $parkId,
        string $authKey
    ) {
        $this->http = $http;
        $this->urlV1 = $urlV1;
        $this->urlV2 = $urlV2;
        $this->clientId = $clientId;
        $this->parkId = $parkId;
        $this->authKey = $authKey;
    }

    /**
     * @return array
     */
    public function drivers(): array
    {
        $drivers = [];
        $response = $this->http->request(
            'POST',
            "{$this->urlV1}/parks/driver-profiles/list",
            [
                'headers' => $this->getHeaders(),
                'json' => [
                    'query' => [
                        'park' => [
                            'id' => $this->parkId
                        ]
                    ]
                ],
            ]
        );
        $message = json_decode($response->getBody()->getContents());
        if (isset($message->driver_profiles) && is_array($message->driver_profiles)) {
            foreach ($message->driver_profiles as $profile) {
                if (isset($profile->accounts) &&
                    is_array($profile->accounts) &&
                    !empty($profile->accounts[0]) &&
                    isset($profile->accounts[0]->id) &&
                    isset($profile->driver_profile)
                ) {
                    $name = isset($profile->driver_profile->first_name) ? (string)$profile->driver_profile->first_name : '';
                    $middleName = isset($profile->driver_profile->middle_name) ? (string)$profile->driver_profile->middle_name : '';
                    $lastName = isset($profile->driver_profile->last_name) ? (string)$profile->driver_profile->last_name : '';

                    $attributes['name'] = "{$lastName} {$name}  {$middleName}";
                    $attributes['yandex_id'] = (string)$profile->accounts[0]->id;
                    $drivers[] = new YandexDriver($attributes);
                }
            }
        }

        return $drivers;
    }

    /**
     * @param Driver $driver
     * @return float
     */
    public function balance(Driver $driver): float
    {
        $balance = 0.0;
        if (!empty($driver->yandex_id)) {
            $response = $this->http->request(
                'POST',
                "{$this->urlV1}/parks/driver-profiles/list",
                [
                    'headers' => $this->getHeaders(),
                    'json' => [
                        'query' => [
                            'park' => [
                                'id' => $this->parkId,
                                'driver_profile' => [
                                    'id' => [
                                        $driver->yandex_id
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            );
            $message = json_decode($response->getBody()->getContents());
            if (isset($message->driver_profiles) && is_array($message->driver_profiles)) {
                foreach ($message->driver_profiles as $profile) {
                    if (isset($profile->accounts) &&
                        is_array($profile->accounts) &&
                        !empty($profile->accounts[0]) &&
                        isset($profile->accounts[0]->balance)
                    ) {
                        $balance = (float)$profile->accounts[0]->balance;
                    }
                }
            }
        }

        return $balance;
    }

    /**
     * @param Driver $driver
     * @param float $transfer
     * @return bool
     */
    public function changeBalance(Driver $driver, float $transfer): bool
    {
        if (!empty($driver->yandex_id)) {
            $response = $this->http->request(
                'POST',
                "{$this->urlV2}/parks/driver-profiles/transactions",
                [
                    'headers' => $this->getHeaders(true),
                    'json' => [
                        'amount' => (string)$transfer,
                        'category_id' => 'partner_service_manual',
                        'description' => "Вывод средств {$transfer}",
                        'driver_profile_id' => (string)$driver->yandex_id,
                        'park_id' => $this->parkId
                    ],
                ]
            );
            if ($response->getStatusCode() == 200) {
                return true;
            }

        }
        return false;
    }

    /**
     * @return string[]
     */
    private function getHeaders(bool $idempotency = false): array
    {
        if ($idempotency) {
            return [
                'X-Client-ID' => $this->clientId,
                'X-Api-Key' => $this->authKey,
                'X-Idempotency-Token' => Uuid::uuid4()->toString()
            ];
        } else {
            return [
                'X-Client-ID' => $this->clientId,
                'X-Api-Key' => $this->authKey
            ];
        }
    }

}