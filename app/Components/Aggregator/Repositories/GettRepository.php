<?php

namespace App\Components\Aggregator\Repositories;


use Carbon\Carbon;

interface GettRepository
{
    public function setToken(string $key): void;

    public function getToken();

    public function getDrivers(bool $force = false): array;

    public function askForReport(Carbon $from, Carbon $to);

    public function getReport(string $uid);

}