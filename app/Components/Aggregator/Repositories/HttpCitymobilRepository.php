<?php

namespace App\Components\Aggregator\Repositories;


use App\Components\Aggregator\Entities\CitymobilDriver;
use App\Components\Driver\Entities\Driver;
use App\Components\Transaction\Entities\Transaction;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Carbon;

class HttpCitymobilRepository implements CitymobilRepository
{
    private $http;
    private $url;
    private $token;

    public function __construct(ClientInterface $http, string $url, string $token)
    {
        $this->http = $http;
        $this->url = $url;
        $this->token = $token;

    }

    public function drivers(): array
    {
        $drivers = [];
        $response = $this->http->request('GET', "{$this->url}/getDriversList?auth_key={$this->token}");
        $message = json_decode($response->getBody()->getContents());
        if (isset($message->data) && is_array($message->data)) {
            foreach ($message->data as $driver) {
                $attributes = [];
                $attributes['name'] = "{$driver->last_name} {$driver->name}  {$driver->middle_name}";
                $attributes['citymobil_id'] = "{$driver->company_outer_id}";
                $drivers[] = new CitymobilDriver($attributes);
            }
        }

        return $drivers;
    }

    public function transactions(Driver $driver): array
    {
        $transactions = [];
        $date_to = Carbon::now()->format('Y-m-d');
        $date_from = Carbon::now()->subDays(5)->format('Y-m-d');

        $response = $this->http->request('GET', "{$this->url}/getDriverPayments?auth_key={$this->token}&id_driver={$driver->citymobil_id}&date_from={$date_from}&date_to={$date_to}&page=1");
        $message = json_decode($response->getBody()->getContents());
        if (isset($message->data) && isset($message->data->payments)  && is_array($message->data->payments) ) {
            foreach ($message->data->payments as $payment) {
                $attributes = [];
                $attributes['driver_id'] = $driver->id;
                $attributes['amount'] = $payment->sum;
                $attributes['aggregator_id'] = 2;
                $attributes['comment'] = $payment->comment;
                $attributes['citymobil_payment_id'] = $payment->id;
                $attributes['citymobil_payment_id_hash'] = $payment->idhash;
                $attributes['citymobil_payment_id_type'] = $payment->id_type;
                $attributes['date'] = $payment->oppdate;
                $transactions[$payment->id] = new Transaction($attributes);
            }
        }

        return $transactions;
    }

    public function balance(Driver $driver): float
    {
        $balance = 0.0;
        if (!empty($driver->citymobil_id)) {
            $response = $this->http->request('POST', "{$this->url}/getDriverData", [
                'json' => [
                    'id_driver' => $driver->citymobil_id,
                    'auth_key' => $this->token
                ]
            ]);
            $message = json_decode($response->getBody()->getContents());
            if (isset($message->data) && isset($message->data->drivers) && is_array($message->data->drivers)) {
                foreach ($message->data->drivers as $driver) {
                    $balance = (float)$driver->balance;
                }
            }
        }
        return $balance;
    }

    public function changeBalance(Driver $driver, int $transfer): bool
    {
        if (!empty($driver->citymobil_id)) {
        $response = $this->http->request('POST', "{$this->url}/changeDriverBalance?auth_key={$this->token}", [
            'json' => [
                'auth_key'=> $this->token,
                'id_driver' => $driver->citymobil_id,
                'type' => $transfer >= 0 ? 'plus' : 'minus',
                'price' => $transfer,
                'description' => 'Изменение баланса'
            ]
        ]);
        $message = json_decode($response->getBody()->getContents());
        if (isset($message->data) && isset($message->data->success) && $message->data->success) {
            return true;
        }
    }
        return false;
    }


}