<?php

namespace App\Components\Telegram\Services;

use GuzzleHttp\ClientInterface;

class Telegram
{
    private $http;
    private $botToken;
    private $botUrl;
    private $botName;
    private $url;

    public function __construct(ClientInterface $http, string $botToken, string $botUrl, string $botName, string $url)
    {
        $this->http = $http;
        $this->botToken = $botToken;
        $this->botUrl = $botUrl;
        $this->botName = $botName;
        $this->url = $url;
    }

    private function getUpdates(): array
    {
        $updates = [];
        $response = $this->http->request('GET', "{$this->botUrl}{$this->botToken}/getUpdates", [
                'timeout' => 60,
                'proxy'  => '207.180.242.44:8080'
            ]

        );
        $message = json_decode($response->getBody()->getContents());
        if (isset($message->result) && is_array($message->result)) {
            $updates = $message->result;
        }

        return $updates;
    }

    public function getChatIdByToken(string $token): string
    {
        $chatId = '';
        $updates = $this->getUpdates();
        foreach ($updates as $update) {
            if (isset($update->message)) {
                if (isset($update->message->text)) {
                    if ($update->message->text === "{$token}" && $token !== '' && $token !== null) {
                        if (isset($update->message->chat) && isset($update->message->chat->id)) {
                            $chatId = $update->message->chat->id;
                        }
                    }
                }

            }
        }

        return $chatId;
    }


    public function link($token): string
    {
        return "{$token}";
    }
}