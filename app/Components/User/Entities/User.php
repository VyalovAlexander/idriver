<?php

namespace App\Components\User\Entities;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'chat_id', 'telegram_token', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Get the user's roles in array.
     *
     * @return string
     */
    public function getRolesNamesAttribute() : array
    {
        return $this->roles->pluck('name')->all();
    }

    public function startTelegram(string $token): void
    {
        $this->chat_id = null;
        $this->telegram_token = $token;
    }

    public function routeNotificationForTelegram()
    {
        return $this->chat_id;
    }

    public function telegramConnected(): bool
    {
        return !empty($this->chat_id);
    }

    public function rolesNames(): array
    {
        $names = $this->cachedRoles()->map(function($item, $key) {
            return $item->name;
        });

        return $names->all();
    }

}
