<?php
namespace App\Components\User\Criteria;

use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class UserNameCriteria implements CriteriaInterface
{
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('name', 'LIKE',  '%' . $this->name . '%');
        return $model;
    }

}