<?php

namespace App\Components\User\Repositories;

use App\Components\User\Entities\User;
use Prettus\Repository\Eloquent\BaseRepository;

class UserRepository extends BaseRepository
{

    function model()
    {
        return User::class;
    }

}