<?php
namespace App\Components\Driver\Criteria;

use App\Support\FilterCriteria;
use Prettus\Repository\Contracts\RepositoryInterface;

class DriverFilterCriteria extends FilterCriteria
{
    protected function filterName(): string
    {
        return 'drivers';
    }

    public function applyValue($model, RepositoryInterface $repository)
    {
        $model = $model->whereIn('driver_id',  explode(",", $this->getValue()));

        return $model;
    }


}
