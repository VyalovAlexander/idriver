<?php
namespace App\Components\Driver\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class DriverUserCriteria implements CriteriaInterface
{
    private $user_ids;

    public function __construct(array $user_ids)
    {
        $this->user_ids = $user_ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereIn('user_id',  $this->user_ids);
        return $model;
    }

}