<?php

namespace App\Components\Driver\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->whenLoaded('user', function () {
                return $this->whenLoaded('user')->name;
            }, ''),
            'phone' => $this->whenLoaded('user', function () {
                return $this->whenLoaded('user')->phone;
            }, ''),
            'email' => $this->whenLoaded('user', function () {
                return $this->whenLoaded('user')->email;
            }, ''),
            'citymobil_id' => $this->citymobil_id,
            'yandextaxi_id' => $this->yandex_id,
            'gett_id' => $this->gett_id,
            'inn' => $this->inn,
            'kpp' => $this->kpp,
            'bank_acnt' => $this->account_number,
            'bank_bik' => $this->bank_bik,
            'payment_purpose' => $this->payment_purpose,
        ];
    }
}