<?php

namespace App\Components\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|unique:users,email|nullable',
            'phone' => 'digits:10|unique:users,phone',
            'name' => 'required|string|min:6|max:250',
            'password' => 'required|min:6|max:250|confirmed',
            'password_confirmation' => 'required'
        ];
    }
}
