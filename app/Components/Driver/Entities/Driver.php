<?php

namespace App\Components\Driver\Entities;

use App\Components\User\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'citymobil_id', 'yandex_id', 'gett_id', 'deposit', 'gett_balance',
        'inn',
        'kpp',
        'bank_bik',
        'payment_purpose',
        'account_number'
    ];

    protected $attributes = [
        'citymobil_id' => '',
        'yandex_id' => '',
        'gett_id' => '',
        'inn' => '',
        'kpp' => '',
        'bank_bik' => '',
        'account_number' => '',
        'payment_purpose' => ''
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }


}
