<?php

namespace App\Components\Driver\Repositories;

use App\Components\Driver\Entities\Driver;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;

class DriverRepository extends BaseRepository
{

    function model()
    {
        return Driver::class;
    }

    function getDriverId(): int
    {
        return $this->findWhere(['user_id' => Auth::user()->getAuthIdentifier()])->first()->id;
    }

    function getDriver(): Driver
    {
        return $this->findWhere(['user_id' => Auth::user()->getAuthIdentifier()])->first();
    }


}