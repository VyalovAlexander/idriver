<?php

namespace App\Components\Common\Criteria;

use App\Support\FilterCriteria;
use Carbon\Carbon;
use Prettus\Repository\Contracts\RepositoryInterface;

class UpdatedAtLessFilterCriteria extends FilterCriteria
{
    protected function filterName(): string
    {
        return 'updated_at_less';
    }

    public function applyValue($model, RepositoryInterface $repository)
    {
        $model = $model->where('date', '<=',  Carbon::createFromFormat('d.m.Y', $this->getValue())->endOfDay());
        return $model;
    }

}