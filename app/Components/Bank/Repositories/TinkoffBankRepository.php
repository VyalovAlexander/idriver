<?php
/**
 * Created by PhpStorm.
 * User: vyalov
 * Date: 02.06.19
 * Time: 14:12
 */

namespace App\Components\Bank\Repositories;

use App\Components\Bank\Entities\Bank;
use App\Components\Bank\Entities\Token;
use App\Components\Claims\Entities\Claim;
use App\Components\Driver\Entities\Driver;
use GuzzleHttp\ClientInterface;

class TinkoffBankRepository implements TinkoffBankRepositoryInterface
{

    private $http;

    public function __construct(ClientInterface $http)
    {
        $this->http = $http;

    }

    public function login(Bank $bank): Token
    {
        $response = $this->http->request('POST', "https://sso.tinkoff.ru/secure/token", [
            'form_params' => [
                'grant_type' => 'refresh_token',
                'refresh_token' => $bank->auth_token,
            ]
        ]);
        $message = json_decode($response->getBody()->getContents());
        if (isset($message->data) && is_array($message->data)) {
            $token = new Token(
                $message->data->access_token,
                $message->data->token_type,
                $message->data->expires_in,
                $message->data->refresh_token,
                $message->data->id_token
            );

            $bank->refresh_token = $message->data->refresh_token;
            $bank->save();

            return $token;
        }
    }

    public function logout(Bank $bank): bool
    {

    }

    public function save(Bank $bank, Token $token, Driver $driver, Claim $claim, float $amount): string
    {
        $response = $this->http->request('POST', "https://sme-partner.tinkoff.ru/api/v1/partner/company", [
            'json' => [
                'documentNumber' => $claim->id,
                'date' => '',
                'amount' => $amount,
                'recipientName' => $driver->user->name,
                'inn' => (string)$driver->inn,
                'kpp' => (string)$driver->kpp,
                'bankAcnt' => $driver->bank_acnt,
                'bankBik' => $driver->bank_bik,
                'accountNumber' => $bank->account_number,
                'paymentPurpose' => $driver->payment_purpose,
                'executionOrder' => 1,
                'taxPayerStatus' => '2',
                'kbk' => '0',
                'oktmo' => '0',
                'taxEvidence' => '0',
                'taxPeriod' => '0',
                'uin' => '0',
                'taxDocNumber' => '0',
                'taxDocDate' => '0'
            ],
            'headers' => [
                "Authorization" => "Bearer {$token->getAccessToken()}",
            ],
        ]);

        $message = json_decode($response->getBody()->getContents());

        return (string)$message->data->documentId;
    }

}