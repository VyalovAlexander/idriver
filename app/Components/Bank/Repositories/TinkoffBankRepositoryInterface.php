<?php

namespace App\Components\Bank\Repositories;

use App\Components\Bank\Entities\Bank;
use App\Components\Bank\Entities\Token;
use App\Components\Claims\Entities\Claim;
use App\Components\Driver\Entities\Driver;

interface TinkoffBankRepositoryInterface
{
    public function login(Bank $bank): Token;

    public function logout(Bank $bank): bool;

    public function save(Bank $bank, Token $token, Driver $driver, Claim $claim, float $amount): string;

}