<?php

namespace App\Components\Bank\Entities;


/**
 * Class Token
 * @package App\Components\Bank\Entities
 */
class Token
{
    /**
     * @var
     */
    private $refresh_token;

    /**
     * @var
     */
    private $access_token;

    /**
     * @var
     */
    private $token_type;

    /**
     * @var
     */
    private $expires_in;

    /**
     * @var
     */
    private $id_token;

    /**
     * Token constructor.
     * @param $refresh_token
     * @param $access_token
     * @param $token_type
     * @param $expires_in
     * @param $id_token
     */
    public function __construct($refresh_token, $access_token, $token_type, $expires_in, $id_token)
    {
        $this->refresh_token = $refresh_token;
        $this->access_token = $access_token;
        $this->token_type = $token_type;
        $this->expires_in = $expires_in;
        $this->id_token = $id_token;
    }

    /**
     * @return mixed
     */
    public function getRefreshToken()
    {
        return $this->refresh_token;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * @return mixed
     */
    public function getTokenType()
    {
        return $this->token_type;
    }

    /**
     * @return mixed
     */
    public function getExpiresIn()
    {
        return $this->expires_in;
    }

    /**
     * @return mixed
     */
    public function getIdToken()
    {
        return $this->id_token;
    }


}