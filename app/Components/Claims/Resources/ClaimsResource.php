<?php

namespace App\Components\Claims\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClaimsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ride_id' => $this->ride_id,
            'created_at' => $this->created_at->format('d.m.Y'),
            'driver_comment' => $this->driver_comment,
            'manager_comment' => $this->manager_comment,
            'citymobil_transfer' => $this->citymobil_transfer,
            'yandextaxi_transfer' => $this->yandex_transfer,
            'gett_transfer' => $this->gett_transfer,
            'common_transfer' => $this->citymobil_transfer + $this->yandex_transfer + $this->gett_transfer,
            'success' => $this->success,
            'closed' => $this->closed,
            'driver_id' => $this->driver_id,
            'driver_name' => $this->whenLoaded('driver', function () {
                return $this->whenLoaded('driver')->user->name;
            }, ''),

        ];
    }
}