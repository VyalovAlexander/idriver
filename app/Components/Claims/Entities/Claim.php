<?php
namespace App\Components\Claims\Entities;


use App\Components\Driver\Entities\Driver;
use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'driver_id',
        'driver_comment',
        'manager_comment',
        'citymobil_transfer',
        'yandex_transfer',
        'gett_transfer',
        'success', 'closed'
    ];

    protected $attributes = [
        'driver_comment' => '',
        'manager_comment' => '',
        'citymobil_transfer' => 0,
        'yandex_transfer' => 0,
        'gett_transfer' => 0,
        'success' => false, 'closed' => false
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function driver() {
        return $this->belongsTo(Driver::class);
    }


}