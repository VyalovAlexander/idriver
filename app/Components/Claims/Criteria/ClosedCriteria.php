<?php
namespace App\Components\Claims\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Support\FilterCriteria;

class ClosedCriteria extends FilterCriteria
{

    protected function filterName(): string
    {
        return 'closed';
    }

    public function applyValue($model, RepositoryInterface $repository)
    {
        $model = $model->where('closed', $this->getValue());
        return $model;
    }

}
