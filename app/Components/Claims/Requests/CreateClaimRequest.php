<?php

namespace App\Components\Claims\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateClaimRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'driver_comment' => 'string|max:255|nullable',
            'citymobil_transfer' => 'required|numeric',
            'yandex_transfer' => 'required|numeric',
            'gett_transfer' => 'required|numeric',
        ];
    }
}
