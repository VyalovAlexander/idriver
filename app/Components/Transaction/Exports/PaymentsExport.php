<?php

namespace App\Components\Transaction\Exports;

use App\Components\Transaction\Repositories\TransactionRepository;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PaymentsExport implements FromView
{

    protected $repository;
    protected $before;
    protected $after;


    public function __construct(TransactionRepository $transactionRepository, Carbon $before, Carbon $after)
    {
        $this->repository = $transactionRepository;
        $this->before = $before;
        $this->after = $after;
    }

    public function view(): View
    {
        $transactions = $this->repository->with(['driver'])->findWhere([
            ['date', '=>', $this->before->startOfDay()->format('Y-m-d H:i:s')],
            ['date', '<=', $this->after->startOfDay()->format('Y-m-d H:i:s')],
        ]);
        return view('components.recruitment.exports.responds-list', ['responds' => $responds]);
    }
}
