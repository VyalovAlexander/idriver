<?php
namespace App\Components\Transaction\Criteria;

use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class DriversCriteria implements CriteriaInterface
{

    private $driver_id;

    public function __construct(int $driver_id)
    {
        $this->driver_id = $driver_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('driver_id', $this->driver_id);
        return $model;
    }

}
