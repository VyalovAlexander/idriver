<?php
namespace App\Components\Transaction\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class PaymentCriteria implements CriteriaInterface
{
    private $onlyPayments;

    public function __construct(bool $onlyPayments)
    {
        $this->onlyPayments = $onlyPayments;
    }
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->onlyPayments) {
            $model = $model->where('station_transaction', true);
        } else {
            $model = $model->where('station_transaction',null);
        }
        return $model;
    }

}
