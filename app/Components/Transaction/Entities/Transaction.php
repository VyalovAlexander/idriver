<?php

namespace App\Components\Transaction\Entities;

use App\Components\Driver\Entities\Driver;
use App\Components\Ride\Entities\Ride;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount', 'aggregator_id', 'driver_id', 'ride_id', 'aggregator_commission', 'station_commission',
        'total', 'comment', 'citymobil_payment_id', 'citymobil_payment_id_hash', 'citymobil_payment_id_type',
        'date', 'ride_tips', 'collected_from_client', 'parking_cost', 'tariff_driver_wo_tips', 'station_transaction'
    ];

    protected $attributes = [
        'aggregator_commission' => 0,
        'amount' => 0,
        'total' => 0,
        'station_commission' => 0,
        'ride_tips' => 0,
        'collected_from_client' => 0,
        'parking_cost' => 0,
        'tariff_driver_wo_tips' => 0
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'date'
    ];

    public function driver() {
        return $this->belongsTo(Driver::class);
    }


    public function ride() {
        return $this->belongsTo(Ride::class);
    }



}
