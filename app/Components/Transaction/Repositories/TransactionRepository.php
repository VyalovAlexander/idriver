<?php

namespace App\Components\Transaction\Repositories;

use App\Components\Aggregator\Criteria\AggreagtorFilterCriteria;
use App\Components\Common\Criteria\UpdatedAtLessFilterCriteria;
use App\Components\Common\Criteria\UpdatedAtMoreFilterCriteria;
use App\Components\Driver\Repositories\DriverRepository;
use App\Components\Transaction\Criteria\DriversCriteria;
use App\Components\Transaction\Criteria\PaymentCriteria;
use App\Components\Transaction\Entities\Transaction;
use App\Support\FilterableRepository;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;


class TransactionRepository extends FilterableRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return Transaction::class;
    }

    protected function filters(): array
    {
        return [
            UpdatedAtLessFilterCriteria::class,
            UpdatedAtMoreFilterCriteria::class,
            AggreagtorFilterCriteria::class,
        ];
    }

    public function currentDriverTransactions(DriverRepository $driverRepository)
    {
        $driver_id = $driverRepository->getDriverId();
        $criteria = new DriversCriteria($driver_id);
        $this->pushCriteria($criteria);
        return $this->orderBy('transactions.date', 'DESC')->paginate();
    }

    public function paymentsReport()
    {

        //$this->pushCriteria(new PaymentCriteria());
        return $this->orderBy('transactions.date', 'DESC')->all();
    }



}