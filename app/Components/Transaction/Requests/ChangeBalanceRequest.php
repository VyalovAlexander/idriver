<?php

namespace App\Components\Transaction\Requests;

use Illuminate\Foundation\Http\FormRequest;


class ChangeBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'driver_id' => 'required|exists:drivers,id',
            'transfer' => 'numeric',
        ];
    }

}
