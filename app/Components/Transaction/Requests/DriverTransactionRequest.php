<?php

namespace App\Components\Transaction\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class DriverTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'aggregators' => 'nullable|digits_between:0,3',
            'updated_at_less' => 'date_format:d.m.Y|nullable',
            'updated_at_more' => 'date_format:d.m.Y|nullable',
        ];
    }

}
