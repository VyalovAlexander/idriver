<?php

namespace App\Components\Transaction\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'updated_at' => $this->date->format('d.m.Y'),
            'amount' => $this->amount,
            'aggregator_id' => $this->aggregator_id,
            'aggregator_name' => filled($this->aggregator_id) ? config('aggregators')[$this->aggregator_id - 1]['name'] : $this->whenLoaded('station', function () {
                return $this->whenLoaded('station')->name;
            }, ''),
            'aggregator_image' => filled($this->aggregator_id) ? config('aggregators')[$this->aggregator_id - 1]['image'] : '',
            'from' => $this->whenLoaded('ride', function () {
                return $this->whenLoaded('ride')->from;
            }, ''),
            'to' => $this->whenLoaded('ride', function () {
                return $this->whenLoaded('ride')->to;
            }, ''),
            'is_citymobil' => filled($this->aggregator_id)? $this->aggregator_id == 2 : false,
            'is_gett' => filled($this->aggregator_id)? $this->aggregator_id == 3 : false,
            'aggregator_commission' => $this->aggregator_commission,
            'station_commission' => $this->station_commission,
            'comment' => $this->comment,
            'total' => $this->total,
            'ride_tips' => $this->ride_tips,
            'collected_from_client' => $this->collected_from_client,
            'is_ride' => (is_null($this->ride_id) || empty($this->ride_id)) ? false : true,
            'driver_id' => $this->driver_id,
            'driver_name' => $this->whenLoaded('driver', function () {
                return $this->whenLoaded('driver')->user->name;
            }, ''),
            'date' => $this->date->format('d.m.Y H:i:s'),
            'parking_cost' => $this->parking_cost,
            'station_transaction' => (bool)$this->station_transaction,
            'tariff_driver_wo_tips' => $this->tariff_driver_wo_tips
        ];
    }
}
