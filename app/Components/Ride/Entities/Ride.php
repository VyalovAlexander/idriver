<?php

namespace App\Components\Ride\Entities;

use App\Components\Aggregator\Utils\AggregatorTrait;
use App\Components\Station\Entities\Station;
use App\Components\User\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    use AggregatorTrait;

    public function driver() {
        $this->hasOne(User::class);
    }

    public function station() {
        $this->hasOne(Station::class);
    }

}
