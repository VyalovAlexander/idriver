<?php

namespace App\Mail;

use App\Components\Claims\Entities\Claim;
use App\Components\User\Entities\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewClaimMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    private $claim;
    private $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Claim $claim, $user)
    {
        $this->claim = $claim;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('notifications.mail.claim.new', [
            'claim' => $this->claim,
            'user' => $this->user
        ])->from('idrivermessages@yandex.ru');
    }
}
