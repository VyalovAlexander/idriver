<?php
namespace App\Support;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

abstract class FilterCriteria implements CriteriaInterface
{

    protected $request;

    abstract protected function filterName() : string;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if (filled($this->request->get($this->filterName()))) {
            return $this->applyValue($model, $repository);
        }
        return $model;
    }

    protected function getValue() {
        return $this->request->get($this->filterName());
    }

    abstract public function applyValue($model, RepositoryInterface $repository);

}