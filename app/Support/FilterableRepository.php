<?php

namespace App\Support;

use Illuminate\Container\Container as Application;
use Illuminate\Http\Request;
use Prettus\Repository\Eloquent\BaseRepository;

abstract class FilterableRepository extends BaseRepository
{

    protected $request;

    public function __construct(Application $app, Request $request)
    {
        parent::__construct($app);
        $this->request = $request;
    }

    abstract protected function filters() : array;

    public function filter(){
        foreach ($this->filters() as $filter) {
            $this->pushCriteria(new $filter($this->request));
        }
    }

}