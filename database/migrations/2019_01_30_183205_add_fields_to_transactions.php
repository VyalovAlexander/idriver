<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->string('comment')->nullable();
            $table->string('citymobil_payment_id')->nullable();
            $table->string('citymobil_payment_id_hash')->nullable();
            $table->string('citymobil_payment_id_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('comment');
            $table->dropColumn('citymobil_payment_id');
            $table->dropColumn('citymobil_payment_id_hash');
            $table->dropColumn('citymobil_payment_id_type');
        });
    }
}
