<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('driver_id');
            $table->string('driver_comment', 255);
            $table->string('manager_comment', 255);
            $table->decimal('citymobil_transfer', 10,2);
            $table->decimal('yandex_transfer', 10,2);
            $table->decimal('gett_transfer', 10,2);
            $table->boolean('success');
            $table->boolean('closed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
