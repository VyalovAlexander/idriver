<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'manager',
                'display_name' => 'Менеджер'
            ],
            [
                'name' => 'admin',
                'display_name' => 'Администратор'
            ],
            [
                'name' => 'driver',
                'display_name' => 'Водитель'
            ]

        ]);
    }
}