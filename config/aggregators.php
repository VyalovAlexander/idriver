<?php

return [
    [
        'id' => 1,
        'name' => 'Яндекс Такси',
        'image' => '/images/aggregators/yandex-taxi-logo.png',
    ],
    [
        'id' => 2,
        'name' => 'Ситимобил',
        'image' => '/images/aggregators/city-mobil-alt.png',
    ],
    [
        'id' => 3,
        'name' => 'Gett Taxi',
        'image' => '/images/aggregators/gett.png',
    ]
];