<?php

return [
    'api_url' => env('GETT_API_URL', 'https://gettpartner.ru/api/fleet/v1'),
    'comfort_login' => env('GETT_API_COMFORT_LOGIN'),
    'comfort_password' => env('GETT_API_COMFORT_PASSWORD'),
    'business_login' => env('GETT_API_BUSINESS_LOGIN'),
    'business_password' => env('GETT_API_BUSINESS_PASSWORD'),
    'comfort_key' => 'comfort',
    'business_key' => 'business'
];