<?php

return [
    'api_url' => env('YANDEX_TAXI_API_URL', 'https://city-mobil.ru/taxiserv/api/partner/1.0.0'),
    'token' => env('YANDEX_TAXI_TOKEN'),
    'api_url_v1' => env('YANDEX_API_URL_V1', 'https://fleet-api.taxi.yandex.net/v1'),
    'api_url_v2' => env('YANDEX_API_URL_V2', 'https://fleet-api.taxi.yandex.net/v2'),
    'client_id' => env('YANDEX_CLIENT_ID'),
    'api_key' => env('YANDEX_API_KEY'),
    'park_id' => env('YANDEX_PARK_ID')

];