<?php

return [
    'api_url' => env('CITYMOBIL_API_URL', 'https://city-mobil.ru/taxiserv/api/partner/1.0.0'),
    'token' => env('CITYMOBIL_TOKEN')
];