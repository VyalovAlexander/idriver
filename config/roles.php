<?php

return [
    'manager' => [
        'id' => 1,
        'name' => 'Менеджер'
    ],
    'admin' => [
        'id' => 2,
        'name' => 'Администратор'
    ],
    'driver' => [
        'id' => 3,
        'name' => 'Водитель'
    ],
];