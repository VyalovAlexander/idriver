<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('command', function () {
    $exitCode = \Illuminate\Support\Facades\Artisan::call('gett:payments');
});

Route::get('/{any}', function () {
    return view('app');
})->where('any', '.*');


//Route::get('me', function () {
//    $response = \Telegram\Bot\Laravel\Facades\Telegram::getMe();
//
//    $botId = $response->getId();
//    $firstName = $response->getFirstName();
//    $userName = $response->getUsername();
//    return response()->json([
//        'botId' => $botId,
//        'firstName' => $firstName,
//        'userName' => $userName
//    ]);
//});
