<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function () {
    Route::group([
        'namespace' => 'Auth',
        'prefix' => 'auth'
    ], function () {
        Route::post('login', 'LoginController@login');
        Route::post('register', 'RegisterController@register');
    });



    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('auth/logout', 'Auth\LoginController@logout');
        Route::get('user/profile', 'User\ProfileController@profile');
        Route::get('aggregators', function () {
            return response()->json(config('aggregators'));
        });

        Route::group([
            'namespace' => 'Telegram',
            'prefix' => 'telegram'
        ], function (){
            Route::post('start', 'TelegramController@start');
            Route::post('verify', 'TelegramController@verify');
            Route::get('connected', 'TelegramController@connected');
        });

        Route::group([
            'namespace' => 'Driver',
            'prefix' => 'driver',
            'middleware' => ['role:driver']
        ], function () {
            Route::get('stations', 'DriverController@stations');
            Route::get('transactions', 'DriverController@transactions');
            Route::get('balance', 'DriverController@balance');
            Route::get('claims', 'ClaimController@index');
            Route::post('claims/create', 'ClaimController@create');
        });

        Route::group([
            'namespace' => 'Manager',
            'prefix' => 'manager',
            'middleware' => ['role:manager']
        ], function () {
            Route::get('stations', 'ManagerController@stations');
            Route::get('drivers', 'DriverController@index');
            Route::get('all-drivers', 'DriverController@all');
            Route::get('transactions', 'ManagerController@transactions');
            Route::post('driver', 'DriverController@create');
            Route::get('driver', 'DriverController@driver');
            Route::get('balance', 'DriverController@balance');
            Route::post('driver/update', 'DriverController@update');
            Route::get('claims', 'ClaimController@index');
            Route::get('claim', 'ClaimController@claim');
            Route::post('claims/update', 'ClaimController@update');
            Route::post('bank/send', 'BankController@sendToBank');
            Route::group([
                'prefix' => 'citymobil',
            ], function () {
                Route::get('drivers', 'DriverController@citymobilDrivers');
                Route::post('balance', 'DriverController@changeCityMobilBalance');
            });
            Route::group([
                'prefix' => 'yandextaxi',
            ], function () {
                Route::get('drivers', 'DriverController@yandextaxiDrivers');
                Route::post('balance', 'DriverController@changeYandexBalance');
            });
            Route::group([
                'prefix' => 'gett',
            ], function () {
                Route::get('drivers', 'DriverController@gettDrivers');
                Route::post('balance', 'DriverController@changeGettBalance');
            });
            Route::group([
                'prefix' => 'reports',
            ], function () {
                Route::get('gett', 'ReportController@gett');
                Route::get('payments', 'ReportController@payments');
            });

        });

        Route::group([
            'namespace' => 'Admin',
            'prefix' => 'admin',
            'middleware' => ['role:admin']
        ], function () {

        });
    });
});

