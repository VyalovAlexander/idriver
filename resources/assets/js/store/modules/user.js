import Vue from 'vue'
import {profile} from '../../api/user'
import {USER_REQUEST, USER_ERROR, USER_SUCCESS, USER_LOGOUT, USER_PROFILE, USER_LOGIN} from '../consts/user'
import { setProfile, resetProfile, getProfile } from "../../helpers/profile"

const state = {
    status: '',
    profile: getProfile() || {
        roles: [],
        name: ''
    },

}

const getters = {
    getProfile: state => state.profile,
    isProfileLoaded: state => !!state.profile.name,
    isDriver: state => state.profile.roles.includes('driver'),
    isManager: state => state.profile.roles.includes('manager'),
    isAdmin: state => state.profile.roles.includes('admin'),
}

const actions = {

}

const mutations = {
    [USER_LOGIN]: (state, profile) => {
        setProfile(profile)
        Vue.set(state, 'profile', profile)
    },
    [USER_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [USER_SUCCESS]: (state, profile) => {
        state.status = 'success'
        Vue.set(state, 'profile', profile)
    },
    [USER_ERROR]: (state) => {
        state.status = 'error'
    },
    [USER_LOGOUT]: (state) => {
        Vue.set(state, 'profile', {
            roles: [],
            name: ''
        })
        resetProfile()
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}