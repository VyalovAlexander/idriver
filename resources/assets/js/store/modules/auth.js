import { AUTH_REQUEST, AUTH_REGISTER,  AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT } from '../consts/auth'
import { USER_REQUEST, USER_LOGOUT, USER_LOGIN } from '../consts/user'
import { login, register,  logout } from '../../api/auth'
import { setAuthToken, resetAuthToken, getToken } from "../../helpers/token"


const state = {
    token: getToken() || '',
    status: '',
    hasLoadedOnce: false,
}

const getters = {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
}

const actions = {
    [AUTH_REQUEST]: ({commit, dispatch}, user) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_REQUEST)
            resetAuthToken()
            login(user.login, user.password)
                .then(resp => {
                    console.log(resp.data.profile)
                    setAuthToken(resp.data.token)
                    commit(USER_LOGIN, resp.data.profile)
                    commit(AUTH_SUCCESS, resp.data.token)
                    resolve()
                })
                .catch(err => {
                    commit(AUTH_ERROR, err)
                    reject(err)
                })
        })
    },
    [AUTH_REGISTER]: ({commit, dispatch}, data) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_REQUEST)
            resetAuthToken()
            register(data.email, data.username, data.password, data.password_confirmation)
                .then(resp => {
                    const token = resp.data.token
                    setAuthToken(token)
                    commit(AUTH_SUCCESS, token)
                    dispatch(USER_REQUEST)
                    resolve(resp.data)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    [AUTH_LOGOUT]: ({commit, dispatch}) => {
        return new Promise((resolve, reject) => {
            logout().then(() => {
                resolve()
            }).finally(() => {
                commit(AUTH_LOGOUT)
                commit(USER_LOGOUT)
                resetAuthToken()
            })
        })
    }
}

const mutations = {
    [AUTH_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [AUTH_SUCCESS]: (state, token) => {
        state.status = 'success'
        state.token = token
        state.hasLoadedOnce = true
    },
    [AUTH_ERROR]: (state) => {
        state.status = 'error'
        state.hasLoadedOnce = true
    },
    [AUTH_LOGOUT]: (state) => {
        state.token = ''
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}