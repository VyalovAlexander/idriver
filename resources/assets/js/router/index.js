import Vue from 'vue'
import Router from 'vue-router'
import {admin, users} from './admin'
import {driver, payment, stations, statistics, claims, claim} from './driver'
import {manager, transactions, reports, drivers, createDriver, updateDriver, driverClaims, updateClaim, paymentReport, gettReport} from './manager'
import {home, login, registration} from './auth'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        home, login, registration,
        driver, payment, statistics,
        manager, transactions, reports, drivers,
        admin, users, createDriver, updateDriver, claim, claims, driverClaims, updateClaim, gettReport, paymentReport
    ],
})