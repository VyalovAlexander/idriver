import store from '../store'
import Driver from '../components/Driver/Driver.vue'
import Statistics from '../components/Driver/Statistics.vue'
import Payment from '../components/Driver/Payment.vue'
import Claims from '../components/Driver/Claims.vue'
import Claim from '../components/Driver/Claim.vue'



const isAllowed = (to, from, next) => {
    if (store.getters.isAuthenticated && store.getters.isDriver) {
        next()
        return
    }
    next('/login')
}

const driver = {
    path: '/driver',
    name: 'Driver',
    component: Driver,
    beforeEnter: isAllowed,
}

const payment = {
    path: '/payment',
    name: 'Payment',
    component: Payment,
    beforeEnter: isAllowed,
}

const statistics = {
    path: '/statistics',
    name: 'Statistics',
    component: Statistics,
    beforeEnter: isAllowed,

}

const claims = {
    path: '/claims',
    name: 'Claims',
    component: Claims,
    beforeEnter: isAllowed,
}

const claim = {
    path: '/claim',
    name: 'Claim',
    component: Claim,
    beforeEnter: isAllowed,
}

export {
    driver,
    payment,
    statistics,
    claim,
    claims
}