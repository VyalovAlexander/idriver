import store from '../store'
import Admin from '../components/Admin/Admin.vue'
import Users from '../components/Admin/Users.vue'

const isAllowed = (to, from, next) => {
    if (store.getters.isAuthenticated && store.getters.isAdmin) {
        next()
        return
    }
    next('/login')
}

const admin = {
    path: '/admin',
    name: 'Admin',
    component: Admin,
    beforeEnter: isAllowed,
}

const users = {
    path: '/users',
    name: 'Users',
    component: Users,
    beforeEnter: isAllowed,
}
export {
    admin,
    users
}