import store from '../store'
import Registration from '../components/Auth/Registration.vue'
import Login from '../components/Auth/Login.vue'
import Home from '../components/Home.vue'

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next()
        return
    }
    next('/')
}

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next()
        return
    }
    next('/login')
}

const home = {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter: ifAuthenticated
}

const login = {
    path: '/login',
    name: 'Login',
    component: Login,
    beforeEnter: ifNotAuthenticated,
}

const registration = {
    path: '/registration',
    name: 'Registration',
    component: Registration,
    beforeEnter: ifNotAuthenticated,
}

export {
    home,
    login,
    registration
}