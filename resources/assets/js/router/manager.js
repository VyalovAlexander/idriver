import store from '../store'
import Manager from '../components/Manager/Manager.vue'
import Drivers from '../components/Manager/Drivers.vue'
import CreateDriver from '../components/Manager/CreateDriver.vue'
import UpdateDriver from '../components/Manager/UpdateDriver.vue'
import Reports from '../components/Manager/Reports.vue'
import Transactions from '../components/Manager/Transactions.vue'
import DriverClaims from '../components/Manager/DriverClaims.vue'
import UpdateClaim from '../components/Manager/UpdateClaim.vue'
import GettReport from '../components/Manager/Reports/GettReport.vue'
import PaymentReport from '../components/Manager/Reports/PaymentsReport.vue'

const isAllowed = (to, from, next) => {
    if (store.getters.isAuthenticated && store.getters.isManager) {
        next()
        return
    }
    next('/login')
}

const manager = {
    path: '/manager',
    name: 'Manager',
    component: Manager,
    beforeEnter: isAllowed,
}

const drivers = {
    path: '/drivers',
    name: 'Drivers',
    component: Drivers,
    beforeEnter: isAllowed,
}

const driverClaims = {
    path: '/driver-claims',
    name: 'DriverClaims',
    component: DriverClaims,
    beforeEnter: isAllowed,
}

const updateClaim = {
    path: '/driver-claim/:id',
    name: 'UpdateClaim',
    component: UpdateClaim,
    props: true,
    beforeEnter: isAllowed,
}

const createDriver = {
    path: '/create',
    name: 'CreateDriver',
    component: CreateDriver,
    beforeEnter: isAllowed,
}

const updateDriver = {
    path: '/update/:id',
    name: 'UpdateDriver',
    component: UpdateDriver,
    props: true,
    beforeEnter: isAllowed,
}


const reports = {
    path: '/reports',
    name: 'Reports',
    component: Reports,
    beforeEnter: isAllowed,
}

const transactions = {
    path: '/transactions',
    name: 'Transactions',
    component: Transactions,
    beforeEnter: isAllowed
}

const gettReport = {
    path: '/gett-report',
    name: 'GettReport',
    component: PaymentReport,
    beforeEnter: isAllowed,
    props: true
}

const paymentReport = {
    path: '/payment-report',
    name: 'PaymentReport',
    component: PaymentReport,
    beforeEnter: isAllowed,
    props: true
}
export {
    manager,
    drivers,
    reports,
    transactions,
    createDriver,
    updateDriver,
    driverClaims,
    updateClaim,
    gettReport,
    paymentReport
}