import {
    DRIVERS,
    CITYMOBIL_DRIVERS,
    CREATE_DRIVER,
    YANDEXTAXI_DRIVERS,
    DRIVER,
    UPDATE_DRIVER,
    CLAIMS,
    UPDATE_CLAIM,
    GETT_DRIVERS,
    CHANGE_CITYMOBIL_BALANCE,
    CHANGE_YANDEXTAXI_BALANCE,
    CHANGE_GETT_BALANCE,
    CLAIM,
    BBALANCE,
    BALANCE,
    ALL_DRIVERS
} from "./routes"

const createDriver = (email, phone, username, password, password_confirmation, citymobil_id, yandextaxi_id, gett_id, inn,
                      kpp,
                      bank_acnt,
                      bank_bik,
                      payment_purpose) => new Promise((resolve, reject) => {
    axios.post(CREATE_DRIVER, {
        'name': username,
        'email': email,
        'phone': phone,
        'password': password,
        'password_confirmation': password_confirmation,
        'citymobil_id': citymobil_id,
        'yandex_id': yandextaxi_id,
        'gett_id': gett_id,
        'inn': inn,
        'kpp': kpp,
        'bank_acnt': bank_acnt,
        'bank_bik': bank_bik,
        'payment_purpose': payment_purpose
    }).then(response => {
        resolve(response)
    }).catch((error) => {
        reject(error)
    });
})

const updateDriver = (id, email, phone, username, password, password_confirmation, citymobil_id, yandextaxi_id, gett_id, inn,
                      kpp,
                      bank_acnt,
                      bank_bik,
                      payment_purpose) => new Promise((resolve, reject) => {
    axios.post(UPDATE_DRIVER, {
        'id': id,
        'name': username,
        'email': email,
        'phone': phone,
        'password': password,
        'password_confirmation': password_confirmation,
        'citymobil_id': citymobil_id,
        'yandex_id': yandextaxi_id,
        'gett_id': gett_id,
        'inn': inn,
        'kpp': kpp,
        'bank_acnt': bank_acnt,
        'bank_bik': bank_bik,
        'payment_purpose': payment_purpose
    }).then(response => {
        resolve(response)
    }).catch((error) => {
        reject(error)
    });
})


const drivers = (page = 1, name = '') => new Promise((resolve, reject) => {
    axios.get(DRIVERS, {
        params: {
            page: page,
            name: name
        }
    }).then(response => {
        resolve({data: response.data.data, page: response.data.meta.current_page})
    }).catch(error => {
        reject(error)
    });
})

const allDrivers = () => new Promise((resolve, reject) => {
    axios.get(ALL_DRIVERS).then(response => {
        resolve(response.data.data)
    }).catch(error => {
        reject(error)
    });
})

const citymobilDrivers = () => new Promise((resolve, reject) => {
    axios.get(CITYMOBIL_DRIVERS).then(response => {
        resolve({drivers: response.data.data})
    }).catch(error => {
        reject(error)
    });
})

const yandextaxiDrivers = () => new Promise((resolve, reject) => {
    axios.get(YANDEXTAXI_DRIVERS).then(response => {
        resolve({drivers: response.data.data})
    }).catch(error => {
        reject(error)
    });
})

const gettDrivers = () => new Promise((resolve, reject) => {
    axios.get(GETT_DRIVERS).then(response => {
        resolve({drivers: response.data.data})
    }).catch(error => {
        reject(error)
    });
})

const driver = (id) => new Promise((resolve, reject) => {
    axios.get(DRIVER, {
        params: {
            id: id
        }
    }).then(response => {
        resolve(response.data.data)
    }).catch(error => {
        reject(error)
    });
})

const claims = (page = 1, closed = false) => new Promise((resolve, reject) => {
    axios.get(CLAIMS, {
            params: {
                page: page,
                closed: closed
            }
        }
    ).then(response => {
        resolve({data: response.data.data, page: response.data.meta.current_page})
    }).catch(error => {
        reject(error)
    });
})

const updateClaim = (id, manager_comment, success) => new Promise((resolve, reject) => {
    axios.post(UPDATE_CLAIM, {
        'id': id,
        'manager_comment': manager_comment,
        'success': success,
    }).then(response => {
        resolve(response)
    }).catch((error) => {
        reject(error)
    });
})

const changeCitymobilBalance = (driver_id, transfer) => new Promise((resolve, reject) => {
    axios.post(CHANGE_CITYMOBIL_BALANCE, {
        'driver_id': driver_id,
        'transfer': transfer,
    }).then(response => {
        resolve(response.data.success)
    }).catch((error) => {
        reject(error)
    });
})

const changeYandextaxiBalance = (driver_id, transfer) => new Promise((resolve, reject) => {
    axios.post(CHANGE_YANDEXTAXI_BALANCE, {
        'driver_id': driver_id,
        'transfer': transfer,
    }).then(response => {
        resolve(response.data.success)
    }).catch((error) => {
        reject(error)
    });
})

const changeGettBalance = (driver_id, transfer) => new Promise((resolve, reject) => {
    axios.post(CHANGE_GETT_BALANCE, {
        'driver_id': driver_id,
        'transfer': transfer,
    }).then(response => {
        resolve(response.data.success)
    }).catch((error) => {
        reject(error)
    });
})

const claim = (id) => new Promise((resolve, reject) => {
    axios.get(CLAIM, {
            params: {
                id: id
            }
        }
    ).then(response => {
        resolve(response.data.data)
    }).catch(error => {
        reject(error)
    });
})

const balance = (id) => new Promise((resolve, reject) => {
    axios.get(BALANCE, {
            params: {
                id: id
            }
        }
    ).then(response => {
        resolve(response.data.data)
    }).catch(error => {
        reject(error)
    });
})

export {
    drivers,
    citymobilDrivers,
    createDriver,
    yandextaxiDrivers,
    driver,
    updateDriver,
    claims,
    updateClaim,
    gettDrivers,
    changeCitymobilBalance,
    changeGettBalance,
    changeYandextaxiBalance,
    claim,
    balance,
    allDrivers
}