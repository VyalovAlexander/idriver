import {PREFIX} from "../prefix"
export const DRIVERS = `${PREFIX}/manager/drivers`
export const CREATE_DRIVER = `${PREFIX}/manager/driver`
export const UPDATE_DRIVER = `${PREFIX}/manager/driver/update`
export const CITYMOBIL_DRIVERS = `${PREFIX}/manager/citymobil/drivers`
export const YANDEXTAXI_DRIVERS = `${PREFIX}/manager/yandextaxi/drivers`
export const GETT_DRIVERS = `${PREFIX}/manager/gett/drivers`
export const DRIVER = `${PREFIX}/manager/driver`
export const CLAIMS = `${PREFIX}/manager/claims`
export const UPDATE_CLAIM = `${PREFIX}/manager/claims/update`
export const CHANGE_CITYMOBIL_BALANCE = `${PREFIX}/manager/citymobil/balance`
export const CHANGE_YANDEXTAXI_BALANCE = `${PREFIX}/manager/yandextaxi/balance`
export const CHANGE_GETT_BALANCE = `${PREFIX}/manager/gett/balance`
export const CLAIM = `${PREFIX}/manager/claim`
export const BALANCE = `${PREFIX}/manager/balance`
export const ALL_DRIVERS = `${PREFIX}/manager/all-drivers`