import {AGGREGATORS} from "./routes"

const aggregators = () => new Promise((resolve, reject) => {
    axios.get(AGGREGATORS).then(response => {
        resolve(response)
    }).catch(error => {
        reject(error)
    });
})

export {
    aggregators,
}