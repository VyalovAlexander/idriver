import {PROFILE_INFO} from "./routes"

const profile = () => new Promise((resolve, reject) => {
    axios.get(PROFILE_INFO).then(response => {
        resolve(response)
    }).catch(error => {
        reject(error)
    });
})

export {
    profile,
}