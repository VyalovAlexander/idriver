import {
    PAYMENTS, GETT
} from "./routes"

const payments = (aggregators = [], date_less = '', date_more = '', only_payments = true, drivers = []) => new Promise((resolve, reject) => {
    axios.get(PAYMENTS , {
            params: {
                aggregators: aggregators.toString(),
                drivers: drivers.toString(),
                updated_at_less: date_less ? date_less.format() : '',
                updated_at_more: date_more ? date_more.format() : '',
                only_payments: only_payments ? "1" : "0"
            }
        }
    ).then(response => {
        console.log(response)
        resolve(response.data.data)
    }).catch(error => {
        reject(error)
    });
})

export {
    payments
}