import {DRIVER_TRANSACTIONS, DRIVER_BALANCE, CLAIMS, CREATE_CLAIM} from "./routes"


const transactions = (page = 1, aggregators = [], date_less = '', date_more = '') => new Promise((resolve, reject) => {
    axios.get(DRIVER_TRANSACTIONS , {
            params: {
                page: page,
                aggregators: aggregators.toString(),
                updated_at_less: date_less ? date_less.format() : '',
                updated_at_more: date_more? date_more.format() : ''
            }
        }
    ).then(response => {
        resolve({data: response.data.data, page: response.data.meta.current_page})
    }).catch(error => {
        reject(error)
    });
})

const balance = () => new Promise((resolve, reject) => {
    axios.get(DRIVER_BALANCE).then(response => {
        resolve(response.data.data)
    }).catch(error => {
        reject(error)
    });
})

const claims = (page = 1, closed = false) => new Promise((resolve, reject) => {
    axios.get(CLAIMS , {
            params: {
                page: page,
                closed: closed
            }
        }
    ).then(response => {
        resolve({data: response.data.data, page: response.data.meta.current_page})
    }).catch(error => {
        reject(error)
    });
})

const createClaim = (driver_comment = '', yandex_transfer = 0, gett_transfer = 0, citymobil_transfer = 0) => new Promise((resolve, reject) => {
    axios.post(CREATE_CLAIM, {
        'driver_comment' : driver_comment,
        'citymobil_transfer' : yandex_transfer,
        'yandex_transfer' : gett_transfer,
        'gett_transfer' : citymobil_transfer
    }).then(response => {
        resolve(response)
    }).catch((error) => {
        reject(error)
    });
})

export {
    transactions,
    balance,
    claims,
    createClaim
}