import {PREFIX} from "../prefix"
export const DRIVER_TRANSACTIONS = `${PREFIX}/driver/transactions`
export const DRIVER_BALANCE = `${PREFIX}/driver/balance`
export const CLAIMS = `${PREFIX}/driver/claims`
export const CREATE_CLAIM = `${PREFIX}/driver/claims/create`