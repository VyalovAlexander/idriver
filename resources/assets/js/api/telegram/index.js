import {START, VERIFY, CONNECTED, LINK} from "./routes"

const start = () => new Promise((resolve, reject) => {
    axios.post(START).then(response => {
        resolve(response.data.link)
    }).catch(error => {
        reject(error)
    });
})

const verify = () => new Promise((resolve, reject) => {
    axios.post(VERIFY).then(response => {
        resolve(response)
    }).catch(error => {
        reject(error)
    });
})

const connected = () => new Promise((resolve, reject) => {
    axios.get(CONNECTED).then(response => {
        resolve(response.data.connected)
    }).catch(error => {
        reject(error)
    });
})

export {
    start,
    verify,
    connected
}