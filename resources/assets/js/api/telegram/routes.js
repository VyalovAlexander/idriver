import {PREFIX} from "../prefix"
export const START = `${PREFIX}/telegram/start`
export const VERIFY = `${PREFIX}/telegram/verify`
export const CONNECTED = `${PREFIX}/telegram/connected`