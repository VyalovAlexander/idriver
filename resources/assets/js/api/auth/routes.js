import {PREFIX} from "../prefix"
export const LOGIN = `${PREFIX}/auth/login`
export const REGISTER = `${PREFIX}/auth/register`
export const LOGOUT = `${PREFIX}/auth/logout`