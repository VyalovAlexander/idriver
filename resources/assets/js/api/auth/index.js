import {LOGIN, REGISTER, LOGOUT, STATIONS} from "./routes"

const login = (login, password) => new Promise((resolve, reject) => {
    axios.post(LOGIN, {
        'login': login,
        'password': password,
    }).then(response => {
        resolve(response)
    }).catch(error => {
        reject(error)
    });
})

const register = (email, username, password, password_confirmation) => new Promise((resolve, reject) => {
    axios.post(REGISTER, {
        'name': username,
        'email': email,
        'password': password,
        'password_confirmation': password_confirmation,
    }).then(response => {
        resolve(response)
    }).catch((error) => {
        reject(error)
    });
})

const logout = () => new Promise((resolve, reject) => {
    axios.post(LOGOUT).then(response => { resolve(response) }).catch(error => { reject(error) });
})


export {
    login,
    register,
    logout,
}