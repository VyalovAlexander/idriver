import {SEND_TO_BANK} from "./routes"


const sendToBank = (claim_id, amount) => new Promise((resolve, reject) => {
    axios.post(SEND_TO_BANK, {
        'id' : claim_id,
        'amount': amount
    }).then(response => {
        resolve(response)
    }).catch((error) => {
        reject(error)
    });
})

export {
    sendToBank
}