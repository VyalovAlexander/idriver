const setProfile = (profile) => {
    "use strict";
    if (window.localStorage) {
        window.localStorage.setItem('profile', JSON.stringify(profile))
    } else {
        document.cookie = "profile=" + JSON.stringify(profile)
    }
}


const resetProfile = () => {
    "use strict";
    if (window.localStorage) {
        window.localStorage.removeItem('profile')
    } else {
        document.cookie = "profile="
    }
}

const getProfile = () => {
    if (window.localStorage) {
        return JSON.parse(window.localStorage.getItem('profile'))
    } else {
        return {"name":"Арсений Роман Дмитриеевич","roles":["manager","driver"]}//JSON.parse(getCookie('profile'))
    }
}

function getCookie(name) {
    let value = "; " + document.cookie;
    let parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}
export {
    setProfile,
    resetProfile,
    getProfile
}