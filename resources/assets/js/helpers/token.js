const setAuthToken = (token) => {
    "use strict";
    if (window.localStorage) {
        localStorage.setItem('user-token', token)
    } else {
        document.cookie = "user-token=" + token
    }
    setAuthHeader()
}

const setAuthHeader = () => {
    if (window.localStorage) {
        const token = localStorage.getItem('user-token')
        if (token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
        }
    } else {
        const token = getCookie('user-token')
        if (token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
        }
    }

}

const getToken = () => {
    if (window.localStorage) {
        return window.localStorage.getItem('user-token')
    } else {
        return getCookie('user-token')
    }
}

const resetAuthToken = () => {
    "use strict";
    if (window.localStorage) {
        localStorage.removeItem('user-token')
    } else {
        document.cookie = "user-token="
    }
    axios.defaults.headers.common['Authorization'] = ''
}

function getCookie(name) {
    let value = "; " + document.cookie;
    let parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

export {
    setAuthToken,
    setAuthHeader,
    resetAuthToken,
    getToken
}