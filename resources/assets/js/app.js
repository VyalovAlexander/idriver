

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'
import router from './router'
import store from './store'
import App from './components/App.vue'
import Vuelidate from 'vuelidate'
import SuiVue from 'semantic-ui-vue'
import 'semantic-ui-css/semantic.min.css'
import {AUTH_LOGOUT} from "./store/consts/auth"
import { setAuthHeader } from "./helpers/token"

Vue.use(SuiVue)
Vue.use(Vuelidate)

const app = new Vue({
    el: '#app',
    router: router,
    store: store,
    created: function () {
        setAuthHeader()
        axios.interceptors.response.use(undefined, function (err) {
            return new Promise(function (resolve, reject) {
                if (err.status === 401 && err.config && !err.config.__isRetryRequest) {
                    // if you ever get an unauthorized, logout the user
                    this.$store.dispatch(AUTH_LOGOUT)
                    this.$router.push('/login')
                    // you can also redirect to /login if needed !
                }
                throw err;
            });
        })
        //if (this.$store.getters.isAuthenticated) {
        //    this.$store.dispatch(USER_REQUEST)
        //}
    },
    render: h => h(App)
});




